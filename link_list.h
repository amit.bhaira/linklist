#ifndef LINK_LIST
#define LINK_LIST

typedef struct Node {
    int data;
    struct Node* next;
} Node;

void append(Node** head, int data);
void printList(Node* head);


#endif
