#include <stdio.h>

#include "link_list.h"

int main() {
    Node* head = NULL;
    append(&head, 1);
    append(&head, 3);
    append(&head, 2);
    printList(head);
    return 0;
}

